import React from "react";
import "./styles/app.css";

import { AiOutlineLinkedin, AiOutlineTwitter } from "react-icons/ai";
import { FiFacebook } from "react-icons/fi";

const App = () => {
  return (
    <main>
      <div className="wrapper">
        <div className="container">
          <div className="centr--box form--box">
            <div className="top-title">
              <h1>Sign Up</h1>
              <span>Please sign to continue out website</span>
            </div>
            <form>
              <div className="input__wrapper">
                <input
                  type="text"
                  name="firstName"
                  placeholder="First Name"
                  required
                />
              </div>
              <div className="input__wrapper">
                <input
                  type="text"
                  name="lastName"
                  placeholder="Last Name"
                  required
                />
              </div>
              <div className="input__wrapper">
                <input
                  type="text"
                  name="email"
                  placeholder="Email Addresss"
                  required
                />
              </div>
              <div className="input__wrapper">
                <input
                  type="password"
                  name="password"
                  placeholder="Password"
                  required
                />
              </div>
              <div className="input__wrapper">
                <input
                  type="tell"
                  name="tell"
                  placeholder="Telphone"
                  required
                />
              </div>
              <div className="radio--button">
                <input type="radio" />
                <span>I accept term & conditions</span>
              </div>
              <div>
                <button className="submit-btn">Submit</button>
              </div>
            </form>
            {/* social icons  */}
            <div className="social-login-wrapper">
              <div>
                <h1>Login with social media</h1>
              </div>
              <div className="icons-wrapper">
                <div>
                  <FiFacebook className="icons" />
                </div>
                <div>
                  <AiOutlineTwitter className="icons" />
                </div>
                <div>
                  <AiOutlineLinkedin className="icons" />
                </div>
              </div>
            </div>
          </div>
          <div className="centr--box image--box">
            <span />
            <h1>LightX</h1>
            <h4>Get started free. Free forever. No credits.</h4>
          </div>
        </div>
      </div>
    </main>
  );
};

export default App;
